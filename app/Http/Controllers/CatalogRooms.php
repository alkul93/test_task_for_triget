<?php

namespace App\Http\Controllers;

use App\CatalogRoom;
use Illuminate\Http\Request;

class CatalogRooms extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CatalogRoom  $catalogRoom
     * @return \Illuminate\Http\Response
     */
    public function show(CatalogRoom $catalogRoom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CatalogRoom  $catalogRoom
     * @return \Illuminate\Http\Response
     */
    public function edit(CatalogRoom $catalogRoom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CatalogRoom  $catalogRoom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CatalogRoom $catalogRoom)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CatalogRoom  $catalogRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(CatalogRoom $catalogRoom)
    {
        //
    }
}
