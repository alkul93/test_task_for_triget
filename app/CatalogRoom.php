<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogRoom extends Model
{
    protected $fillable = [
        'number', 'title', 'description'
    ];
}
