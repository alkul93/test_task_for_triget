<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservedRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserved_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catalog_room_id')->notNull()->unsigned()->comment('Ссылка на номер, который забронировали');
            $table->string('user_name')->comment('ФИО пользователя');
            $table->integer('phone_number')->comment('Номер телефона клиента');
            $table->integer('date_from')->comment('Время, с которого номер бронируется');
            $table->integer('date_to')->comment('Время, до которого номер бронируется');
            $table->timestamps();

            $table->foreign('catalog_room_id')->references('id')->on('catalog_rooms');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reserved_rooms', function (Blueprint $table) {
            $table->dropForeign(['catalog_room_id']);
        });
        Schema::dropIfExists('reserved_rooms');
    }
}
